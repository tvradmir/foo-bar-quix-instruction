import { TestBed } from '@angular/core/testing';
import { FooBarQuixService } from './foo-bar-quix.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
describe('FooBarQuixService', () => {

  let service: FooBarQuixService;
  let httpTestingController: HttpTestingController;
  const fooBarQuixUrl = '/foo-bar-quix/';
  const serverUrl = 'http://server:8080';


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: 'SERVER_URL', useValue: serverUrl },
        FooBarQuixService]
    });
    service = TestBed.inject(FooBarQuixService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });
 
  it("should call HttpClient.GET method with backend url and parameter 1 when convert 1 ", () =>{

    let inputNumber = 1;

    service.convert(inputNumber).subscribe();

    const req = httpTestingController.expectOne(serverUrl+fooBarQuixUrl+inputNumber);
    expect(req.request.method).toBe("GET")

  } )


  it("should call HttpClient.GET method with backend url and parameter 2 when convert 2 ", () =>{

    let inputNumber = 2;

    service.convert(inputNumber).subscribe();

    const req = httpTestingController.expectOne(serverUrl+fooBarQuixUrl+inputNumber);
    expect(req.request.method).toBe("GET")
  } )


  
  afterEach(() => {
    httpTestingController.verify();
  });


});
