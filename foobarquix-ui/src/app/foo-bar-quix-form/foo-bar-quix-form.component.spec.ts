import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { FooBarQuixFormComponent } from './foo-bar-quix-form.component';

describe('FooBarQuixFormComponent', () => {
  let component: FooBarQuixFormComponent;
  let fixture: ComponentFixture<FooBarQuixFormComponent>;
  let nativeElement : any;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [FooBarQuixFormComponent]
    });
    fixture = TestBed.createComponent(FooBarQuixFormComponent);
    component = fixture.componentInstance;
    nativeElement = fixture.debugElement.nativeElement;

  });

  it('input invalid when empty', () => {
    expect(component.inputFormControl.valid).toBeFalsy();
  });


  it('input invalid when is 1', () => {
    component.inputFormControl.setValue(1)
    expect(component.inputFormControl.valid).toBeTruthy();
  });


  it("should emit event to FooBarQuixComponent  with 1 when input is 1", () => {
    component.inputFormControl.setValue(1)

    let input : number
    component.submitNumberOutput.subscribe( (val : number) => input = val )

    component.submitNumber();

    expect(input).toBe(1);


  })


  it("should reset input value after submit a number", () => {
    component.inputFormControl.setValue(1)

    component.submitNumber();

    expect(component.inputFormControl.value).toBeNull()



  })

  it('should render inputFormControl elements', () => {
    const inputNumber = nativeElement.querySelector('input[id="inputFormControl"]');

    expect(inputNumber).toBeTruthy();
  });

  it('should render label elements with content: Number to convert', () => {
    const inputNumber : HTMLElement = nativeElement.querySelector('label');

    expect(inputNumber.textContent).toBeTruthy("Number to convert");
  });


  it('should render button elements with content: Convert', () => {
    const inputNumber : HTMLElement = nativeElement.querySelector('button');

    expect(inputNumber.textContent).toBeTruthy("Convert");
  });

  it('should call method submitNumber', fakeAsync(() => {
    spyOn(component, 'submitNumber');
    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    tick();
    expect(component.submitNumber).toHaveBeenCalled();
  
  }));

  it('should  emit event', fakeAsync(() => {
    spyOn(component.submitNumberOutput, 'emit');
    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    tick();
    expect(component.submitNumberOutput.emit).toHaveBeenCalled();
  
  }));
});
