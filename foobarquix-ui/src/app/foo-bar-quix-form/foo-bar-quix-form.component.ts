import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-foo-bar-quix-form',
  templateUrl: './foo-bar-quix-form.component.html'
})
export class FooBarQuixFormComponent implements OnInit {
  inputFormControl: FormControl = new FormControl('', [ Validators.required]);
  
  @Output()
  submitNumberOutput: EventEmitter<FormControl> = new EventEmitter<FormControl>();;


  constructor() {

  }

  ngOnInit(): void {
  }

  submitNumber(): void {
    this.submitNumberOutput.emit(this.inputFormControl.value)
    this.inputFormControl.reset()
  }

}
