import { FooBarQuixService } from '../foo-bar-quix.service';
import { FooBarQuixComponent } from './foo-bar-quix.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Result } from '../model/result';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import * as Rx from 'rxjs';

const _OBJECT: Result = new Result();
_OBJECT.result ="1";
class FooBarQuixServiceStub  {
  
  public convert(input: number): Rx.Observable<Result> {
    return Rx.of(_OBJECT);
  }
}

describe('FooBarQuixComponent', () => {

  let component: FooBarQuixComponent ;
  let fixture: ComponentFixture<FooBarQuixComponent>;
  let fooBarQuixService : FooBarQuixService
  
 beforeEach(() => {
  TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [
      { provide: FooBarQuixService, useClass: FooBarQuixServiceStub },
    ]
  });
  fixture = TestBed.createComponent(FooBarQuixComponent);
  component = fixture.componentInstance;
  fooBarQuixService = fixture.debugElement.injector.get(FooBarQuixService);

});

it('should call FooBarQuixService.convert method with parameter 1 when convertNumber 1',  () => {

  const inputNumber = 1;

  spyOn(fooBarQuixService, 'convert')
  .withArgs(inputNumber).and
  .returnValue(Rx.of(new Result())); 

  component.convertNumber(inputNumber)
  expect(fooBarQuixService.convert).toHaveBeenCalledWith(inputNumber);
});

it('should convert number 1 and return response as array of { numberToConvert: 1 ,result: "1"}', () => {
  
  const inputNumber = 1;
 
  component.convertNumber(inputNumber)
  expect(component.results).toEqual([{ numberToConvert: 1 ,result: "1"}]);
});

});
