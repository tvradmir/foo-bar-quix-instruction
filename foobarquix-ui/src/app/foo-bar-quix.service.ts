import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Result } from './model/result';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FooBarQuixService {

  fooBarQuixUrl = '/foo-bar-quix/'
  convert(input: number) : Observable<Result>{
    return this.http.get<Result>(this.serverUrl+this.fooBarQuixUrl+input)
  }
  constructor(private http : HttpClient, @Inject('SERVER_URL') private serverUrl: string) { }

}
