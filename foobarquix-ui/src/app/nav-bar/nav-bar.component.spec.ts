import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NavBarComponent } from './nav-bar.component';

describe('NavBarComponent', () => {


  it(`navItemscontain {text: 'foo-bar-quix', path: 'foo-bar-quix'} `, () => {
    let component: NavBarComponent = new NavBarComponent();
    expect(component.navItems).toContain({text: 'foo-bar-quix', path: 'foo-bar-quix'});
  });
});
