package com.kata.foobarquix.services

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

/**
 * @project foobarquix-back
 * @author Vlado on 30/08/2021
 */
class FooBarQuixServiceTest{

    private lateinit var fooBarQuixService: FooBarQuixService

    @BeforeEach
    internal fun setUp() {
         fooBarQuixService = FooBarQuixService()
    }

    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("1, 1", "2, 2", "4, 4")
    fun convertNoFooBarQuixNumber(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }

    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("6, Foo", "9, Foo", "12, Foo")
    fun convertNumberDivisibleByThreeNotContainingThreeFiveAndSeven(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }

    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("10, Bar", "20, Bar")
    fun convertNumberDivisibleByFiveNotContainingThreeFiveAndSeven(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }

    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("60, FooBar","90, FooBar")
    fun convertNumberDivisibleByThreeAndFiveNotContainingThreeFiveAndSeven(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }

    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("13, Foo","31, Foo", "431, Foo","133, FooFoo", "331, FooFoo", "3331, FooFooFoo")
    fun convertNumberContainsThreeNotDivisibleThreeAndFive(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }


    @ParameterizedTest(name = "conversion of {0} should be Foo")
    @CsvSource("3, FooFoo", "36, FooFoo", "33, FooFooFoo")
    fun convertNumberContainsThreeAndDivisibleByThreeNotDivisibleFive(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }

    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("52, Bar", "151, Bar", "551, BarBar")
    fun convertNumberContainsFiveNotDivisibleThreeAndFive(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }

    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("5, BarBar","25, BarBar")
    fun convertNumberContainsFiveDivibleByFiveAndNotDivisibleThree(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }


    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("51, FooBar")
    fun convertNumberContainsFiveDivibleByThree(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }

    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("35, BarFooBar", "320, BarFoo")
    fun convertNumberContainsThreeAndDivisibleByFive(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }


    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("7, Quix", "17, Quix", "77,QuixQuix")
    fun convertNumberContainsSevenNotDivisibleThreeAndFive(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }


    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("27, FooQuix","72, FooQuix")
    fun convertNumberContainsSevenAndDivisibleThree(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }


    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("70, BarQuix")
    fun convertNumberContainsSevenAndDivisibleFive(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }


    @ParameterizedTest(name = "conversion of {0} should be {1}")
    @CsvSource("75, FooBarQuixBar")
    fun convertNumberContainsSevenFiveAndDivisibleFiveandThree(inputNumber: Int, expectedValue: String) {
        assertThat(fooBarQuixService.convertNumber(inputNumber)).isEqualTo(expectedValue)
    }




}