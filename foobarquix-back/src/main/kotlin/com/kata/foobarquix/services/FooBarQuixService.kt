package com.kata.foobarquix.services

import org.springframework.stereotype.Component

@Component
class FooBarQuixService {

    private val DIVISORS_VALUES = listOf(3, 5)


    private val FOO_BAR_QUIX = mapOf(3 to "Foo", 5 to "Bar", 7 to "Quix")

    private val EMPTY_STRING = ""

    fun convertNumber(inputNumber: Int): String{

       return buildResultFromDivisors(inputNumber)
            .plus(buildResultByContent(inputNumber))
            .ifBlank { inputNumber.toString()}

    }

    private fun buildResultByContent(inputNumber: Int) =
        inputNumber.toString().replace(buildRegexValue(), EMPTY_STRING)
            .map { i ->
                FOO_BAR_QUIX[i.toString().toInt()]
            }.joinToString(EMPTY_STRING)

    private fun buildResultFromDivisors(inputNumber: Int) = DIVISORS_VALUES
        .filter { divisor -> isDivisibleBy(inputNumber, divisor) }
        .joinToString(EMPTY_STRING) { divisor -> FOO_BAR_QUIX[divisor].toString() }

    private fun buildRegexValue() = FOO_BAR_QUIX.keys.joinToString(EMPTY_STRING,"[^","]").toRegex()



    private fun isDivisibleBy(dividend: Int, divisor: Int) = dividend % divisor == 0

}